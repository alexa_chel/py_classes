class Animal:
    def __init__(self, name="noname", voice="Голос", weight=0.0):
        self.name = name
        self.voice = voice
        self.weight = weight

    def feed(self):
        print(f"Животное \"{self.name}\" накормлено")

    def voiceOut(self):
        print(self.voice)

    def getName(self):
        print(self.name)


class Goose(Animal):
    def collectEggs(self):
        print("Яйца собраны")


class Cow(Animal):
    def milk(self):
        print("Дойка завершена")


class Sheep(Animal):
    def cut(self):
        print("Стрижка завершена")


class Chicken(Animal):
    def collectEggs(self):
        print("Яйца собраны")


class Goat(Animal):
    def milk(self):
        print("Дойка завершена")


class Duck(Animal):
    def collectEggs(self):
        print("Яйца собраны")


goose1 = Goose("Серый", "Га-га", 3.3)
goose2 = Goose("Белый", "Га-га", 2.5)
cow = Cow("Манька", "Мууу", 500)
sheep1 = Sheep("Барашек", "Беее", 80)
sheep2 = Sheep("Кудрявый", "Беее", 46)
chicken1 = Chicken("Ко-Ко", "Ко-ко-ко", 3.5)
chicken2 = Chicken("Кукареку", "Ко-ко-ко", 3.7)
goat1 = Goat("Рога", "Меее", 130)
goat2 = Goat("Копыта", "Меее", 90)
duck = Duck("Кряква", "Кря-кря", 1.4)

goose1.feed()
goat1.getName()
goat1.feed()
goat2.getName()
cow.voiceOut()
cow.milk()
duck.getName()

animals = [goose1, goose2, cow, sheep1, sheep2, chicken1, chicken2, goat1, goat2, duck]
weight = 0
heavy_animal = animals[0]

for animal in animals:
    weight += animal.weight

    if animal.weight > heavy_animal.weight:
        heavy_animal = animal

print(f"Вес всех животных = {weight}кг")
print("Самое тяжелое животное:")
heavy_animal.getName()

